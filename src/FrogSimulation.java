public class FrogSimulation {
    private int goalDistance;
    private int maxHops;

    public int[] testHops;
    public int nextHopIndex;

    public FrogSimulation(int dist, int numHops) {
        this.goalDistance = dist;
        this.maxHops = numHops;
    }

    private int hopDistance() {
        return this.testHops[this.nextHopIndex++ % this.testHops.length];
    }

    public boolean simulate() {
        int hop = 0;
        int tDistance = 0;

        while (tDistance < this.goalDistance && hop < this.maxHops && tDistance > -1) {
            hop = hop + 1;
            tDistance = tDistance + this.hopDistance();
        }
        return tDistance >= this.goalDistance;
    }

    public double runSimulations(int num) {
        double n = 0;
        for (int i = 0; i < num; i++) {
            if (this.simulate()) n++;
        }
        return n / num;
    }
}
